namespace WindowServices.Service.Command
{
    public interface ICommand
    {
        void Execute();
        void RollBack();
    }
}