﻿using System;
using System.Collections.Generic;
using WindowServices.Server.Interface;
using WindowServices.Service.Properties;

namespace WindowServices.Service.Command.Task
{
    internal sealed class LoadServiceTask : TaskBase
    {
        private readonly Dictionary<string, ServiceHelper> _services;
        public LoadServiceTask(Dictionary<string, ServiceHelper> services, ServiceEntityEx se)
            : base(se)
        {
            _services = services;
        }

        public LoadServiceTask(Dictionary<string, ServiceHelper> services, ServiceEntityEx se, int order)
            : base(se, order)
        {
            _services = services;
        }
        protected override void RollBack(ServiceEntityEx se)
        {
            if (_services.ContainsKey(se.Key))
            {
                _services.Remove(se.Key);
            }
        }

        protected override void Execute(ServiceEntityEx se)
        {
            var sh = Utility.LoadService(se);
            if (sh == null)
            {
                throw new Exception(string.Format(Resources.LoadServiceError, se.Name,""));
            }

            _services.Add(se.Key, sh);
        }


    }
}