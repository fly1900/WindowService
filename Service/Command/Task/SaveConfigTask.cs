using WindowServices.Server.Interface;

namespace WindowServices.Service.Command.Task
{
    internal sealed class SaveConfigTask : TaskBase
    {
        public SaveConfigTask(ServiceEntityEx se, int order)
            : base(se, order)
        {
        }

        public SaveConfigTask(ServiceEntityEx se)
            : base(se)
        {
        }

        protected override void RollBack(ServiceEntityEx se)
        {
            Common.ServiceHelper.DelServiceEntity(se.Id, se.Version);
        }

        protected override void Execute(ServiceEntityEx se)
        {
            Common.ServiceHelper.AddServiceEntity(se);
        }
    }
}