﻿#region

using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using WindowServices.Interface;

#endregion

namespace EmptyService
{
    [Description("Test Service.")]
    [DisplayName("EmptyService")]
    [Guid("677FD1A6-9D73-4835-8514-402C2ACBE739")]
    [Serializable]
    public class EmptyService : IService
    {
        #region IService Members

        private ServiceEntity _se;

        public void Initialize(ServiceEntity serviceEntity)
        {
            _se = serviceEntity;
        }

        private bool _isRunning;

        public void Start()
        {
            _isRunning = true;
            while (_isRunning)
            {
                try
                {
                    LogServiceHelper.LogHelper.Logger.InfoFormat("Version.new:ID={0},Name={1},Type={2},Version={3}\n", _se.Id, _se.Name, _se.Type,
                                                     _se.Version);
                }
                catch
                {
                }
                Thread.Sleep(5000);
            }
        }

        public void Stop()
        {
            _isRunning = false;
        }

        public void Pause()
        {
        }

        #endregion
    }
}